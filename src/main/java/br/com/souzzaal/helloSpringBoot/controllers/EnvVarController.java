package br.com.souzzaal.helloSpringBoot.controllers;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EnvVarController {

    // ENV_VAR = injetada da variavel de ambiente : NENHUMA = valor padrao a ser exibido caso a variavel de ambiente nao seja exista.
    @Value("${ENV_VAR:NENHUMA}")
    private String envVar;

    @GetMapping("/env")
    public String getAppMessage() {
        return "O conteudo da variavel de ambiente: " + envVar;
    }
}

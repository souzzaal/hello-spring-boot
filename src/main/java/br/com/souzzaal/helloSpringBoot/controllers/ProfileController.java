package br.com.souzzaal.helloSpringBoot.controllers;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProfileController {

    @Value("${app.message}") // faz a injecao da propriedade existente no arquivo de configuracoa para a variavel
    private String appMessage;

    @GetMapping("/profile")
    public String getAppMessage() {
        return appMessage;
    }
}

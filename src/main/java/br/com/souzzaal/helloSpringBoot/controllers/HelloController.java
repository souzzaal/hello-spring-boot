package br.com.souzzaal.helloSpringBoot.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController // idendifica o controller como Rest
public class HelloController {

    @GetMapping("/") // faz o mapeamento para a rota http://localhost:8080
    public String helloMessage()
    {
        return "Hello, Spring Boot";
    }
}

# Spring Boot

O Spring boot facilita o desenvolvimento de aplicações simplificando a configuração de projetos Spring, eliminando a necessidade de criação manual de arquivos de configuração. Ao adicionar uma dependência o Sprint Boot realiza a auto configuração.

## Para criar um projeto:

1.  Acessar https://start.spring.io
2.  Preencher os dados do projeto (para este projeto foram usadas as seguintes configurações)
	3. Project: maven
	4. 	Language: Java
	5. 	SpringBoot: 2.3.1
	6. 	Packaging: Jar
	7. 	Java: 14
8.  Adicionar dependencias (para este exemplo foram usadas as seguintes dependências)
	9. 	WEB (Spring Web)
10.  Clicar em *generate* para baixar o projeto.

## Desenvolvendo o hello world

Dentro do pacote `controlers` criou-se a classe `HelloController` com um método `helloMessage`, que foi mapeado para a rota `/`.

## Executando o projeto

O Spring Boot provê um servidor web embutido, logo para ver a aplicação funcionando basta acessar  a pasta do projeto e executar `mvn boot-spring:run` via terminal.
Em seguida, acessar a http://localhost:8080 no browser.

## Auto Configuration
O auto configuration é um dos principais recursos do Spring Boot: detecta e configura 
as dependências do projeto de forma transparente para nós.

A anotação @SpringBootApplication automaticamente adiciona para nós o recurso do auto configuration.

O Auto Configuration faz parte do Spring Boot, e funciona mais ou menos da seguinte maneira: ao adicionar uma dependência (para este exemplo, no pom.xml), em tempo de execução, ao subir o projeto, o auto configuration adiciona as dependências do projeto.

### Fatjar/Uberjar

Uma das grandes vantagens do Spring Boot é, ao fazer o build do projeto, ter à disposição um projeto pronto para deploy em ambientes de teste, desenvolvimento e/ou produção.

O fatjar/uberjar, é um pacote, gerado no build, que contem além do código, todas as dependências e arquivos de configuração necessários para disponibilizar a aplicação.

Feito isso, é possível subir a aplicação apenas executando  `java -jar seuApp.jar`.

Como esse pacote é autossuficiente não é necessário ter um servidor para testar/subir a aplicação.

## Spring Boot Profiles

Permite criar arquivos de configuração específicos para cada ambiente desejado: dev, testing, production, etc.
Para isto, basta adicionar novos arquivos em src/main/resources, como, por exemplo `application-dev.yml` ou `application-prod.properties`.

Para exemplo, criou-se o arquivo `src/main/java/.../cofig/DbConfiguration`, o qual fara o mapeamento do arquivo de propriedades para a classe java através de annotations.

O ambiente de configuração ativo por padrão deve ser definido no arquivo `application.properties` através de `spring.profiles.active=dev`(que neste caso define o ambiente padrão para dev).

Para testar o funcionamento dos profiles foi criada a classe  `src/main/java/.../controllers/ProfileController`para a qual a rota `/profile` está mapeada.

É possível sobrescrever, em tempo de execução, o valor das propriedades, passando-a via terminal.

Por exemplo: `mvn spring-boot:run -Dspring-boot.run.profiles="prod"` força que a aplicação seja inicializada com a configuração de `application-prod.properties`.

### Variáveis de ambiente

O Spring Boot permite que variáveis de ambiente sejam injetadas como se fossem propriedades. Para isso deve-se fazer uso da anotação @value como apresentado em `src/main/java/.../controllers/EnvVarController`.

Ao executar, no linux, `export ENV_VAR=ValorEnvVar && mvn spring-boot:run` e acessar a rota `/env` a aplicação exibirá o conteúdo de ENV_VAR.